# README #

### What is this repository for? ###

This repository contains a (badly implemented) simulation setup for two vesicles with adhesion. 
The pre smoothing is done individually for each vesicle, see precase_upper and precase_lower.
The phase fields at the end of these simulations are to be inserted as the initial configuration of the main simulation (rename to C1, C2 respectively).
