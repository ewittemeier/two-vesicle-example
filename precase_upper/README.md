
### Description ###

Discocyte-pre-case is an application to vesicleFoam and it is
necessary to be run before the main case. The result is a 
smooth vesicle starting by a sharp one.

### local enviroment running ###

    $ ./Allrun

For cleaning run:

    $ ./Allclean
    

### RWTH cluster running ###

To run the test case on the RWTH cluster run:


    $ sbatch cluster.sh

For cleaning run:

    $ ./Allclean
    
For checking the already submitted jobs:

    $ squeue -u $USER
    

### Cluster Submiting MPI Job ###

set the runtime limit 

    #SBATCH --time=0-23:00:00

ask for 4 tasks

    #SBATCH --ntasks=4
    
ask for 1 GB memory per task

    #SBATCH --mem-per-cpu=1024M   #M is the default and can therefore be omitted, but could also be K(ilo)|G(iga)|T(era)

name the job

    #SBATCH --job-name=torus

declare the merged STDOUT/STDERR file

    #SBATCH --output=output.%J.txt

for more details visit:
https://doc.itc.rwth-aachen.de/display/CC/Submitting+a+job